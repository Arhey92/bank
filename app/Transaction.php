<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Kyslik\ColumnSortable\Sortable;

class Transaction extends Model
{
    use Sortable;

    protected $fillable = [
        'customer_id', 'amount'
    ];

    public $sortable = [
        'amount',
        'created_at',
        'customer_id'
    ];

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'id', 'customer_id');
    }

    public static function apply(Request $filters)
    {
        $transaction = (new Transaction)->newQuery();

        if ($filters->has('customer_id')) {
            $transaction->where('customer_id', $filters->input('customer_id'));
        }

        if ($filters->has('amount')) {
            $transaction->where('amount', $filters->input('amount'));
        }

        if ($filters->has('date')) {
            $transaction->whereDate('created_at', $filters->input('date'));
        }

        if($filters->has('limit')){
            $limit = $filters->input('limit');
        }else{
            $limit = Transaction::all()->count();
        }

        $transaction->limit($limit);
        if($filters->has('offset')){
            $transaction->offset($filters->input('offset'));
        }

        return $transaction->get();
    }
}
