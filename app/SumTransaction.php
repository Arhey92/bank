<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SumTransaction extends Model
{
    protected $fillable = [
        'sum', 'from_date', 'to_date'
    ];
}
