<?php

namespace App\Http\Controllers;

use App\SumTransaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StoreSumTransactions extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $sum = DB::table('transactions')->whereBetween('created_at', [Carbon::yesterday(), Carbon::today()])->sum('amount');
        SumTransaction::create([
            'sum' => $sum
        ]);

        return $sum;
    }
}
