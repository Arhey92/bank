<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginate = 5;
        if(isset($_GET['paginate'])){
            $paginate = $_GET['paginate'];
        }

        $transactions = Transaction::sortable()->paginate($paginate);

        return view('home', ['transactions' => $transactions]);
    }
}
