@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Transactions</div>
                <div class="card-header">Paginate:
                    <a href="{{url('/home?page='.$transactions->currentPage().'&paginate=5')}}">5</a>
                    <a href="{{url('/home?page='.$transactions->currentPage().'&paginate=10')}}">10</a>
                    <a href="{{url('/home?page='.$transactions->currentPage().'&paginate=50')}}">50</a>
                </div>

                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">@sortablelink('customer_id', 'Customer_id')</th>
                            <th scope="col">@sortablelink('amount', 'Amount')</th>
                            <th scope="col">@sortablelink('created_at', 'Date')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($transactions as $transaction)
                            <tr>
                                <th scope="row">{{$transaction->increment}}</th>
                                <td>{{$transaction->customer_id}}</td>
                                <td>{{$transaction->amount}}</td>
                                <td>{{$transaction->created_at->format('d-m-Y H:i')}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {{$transactions->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
