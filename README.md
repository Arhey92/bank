# Bank #
### Start project: ###

- Clone project
- Create .env file and fill it
Than call next commands in terminal:
- composer update
- php artisan key:generate
- composer dump-autoload
- php artisan migrate --seed
- php artisan passport:install

For Cron you must to add the following Cron entry to your server.
Time in task: every 2 days at 23:47.

```text
47 23 */2 * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
```

This Cron will call the Laravel command scheduler every 2 days at 23:47.
Laravel calls scheduled tasks, which storing sum transactions of previous day to 'sum_transactions' table.
Call schedule tasks in App/Console/Kernel.php with cron time.
See Wiki for all information about API.
